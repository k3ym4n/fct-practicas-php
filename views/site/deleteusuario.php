<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<h1> Formulario Eliminar Usuario</h1>
<h3><?= $msg ?></h3>
<?php 
    $form = ActiveForm::begin([
        'method' => "post",
        'enableClientValidation' => true,
        
    ]);
?>


<div class ="form-group">
	<?= $form->field($model, "nombre")->input("text") ?>
</div>

<div class = "form-group">
	<?= $form->field($model, "apellido")->input("text")?>
</div>

<div class = "form-group">
	<?= $form->field($model, "empresa")->input("text")?>
</div>

<div class = "form-group">
	<?= $form->field($model, "capital")->input("text")?>
</div>

<div class = "form-group">
	<?= $form->field($model, "contrasena")->input("text")?>
</div>

<?= Html::submitButton("Eliminar" ,["class"=> "btn btn-primary"])?>

<?php  $form->end() ?>

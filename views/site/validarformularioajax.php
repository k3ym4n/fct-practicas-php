<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<h1> Validar Formularios AJAX</h1>
<?php 
    $form = ActiveForm::begin([
        'method' => "post",
        'id' => "formulario",
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
    ]);
?>

<div class ="form-group">
	<?= $form->field($model, "nombre")->input("text") ?>
</div>

<div class = "form-group">
	<?= $form->field($model, "email")->input("text")?>
</div>

<?= Html::submitButton("Enviar" ,["class"=> "btn btn-primary"])?>

<?php  $form->end() ?>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>

<a href="<?= Url::toRoute("site/viewusuario") ?>">Ir a la lista de alumnos</a>
<h1> Formulario crear Usuario</h1>
<h3><?= $msg ?></h3>
<?php 
    $form = ActiveForm::begin([
        'method' => "post",
        'enableClientValidation' => true,
        
    ]);
?>


<div class ="form-group">
	<?= $form->field($model, "nombre")->input("text") ?>
</div>

<div class = "form-group">
	<?= $form->field($model, "apellido")->input("text")?>
</div>

<div class = "form-group">
	<?= $form->field($model, "empresa")->input("text")?>
</div>

<div class = "form-group">
	<?= $form->field($model, "capital")->input("text")?>
</div>

<div class = "form-group">
	<?= $form->field($model, "contrasena")->input("text")?>
</div>

<?= Html::submitButton("Enviar" ,["class"=> "btn btn-primary"])?>

<?= Html::submitButton("Eliminar" ,["class"=> "btn btn-primary"])?>

<?php  $form->end() ?>


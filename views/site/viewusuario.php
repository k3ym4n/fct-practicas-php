<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\data\Pagination;
use yii\widgets\LinkPager;

?>

<a href="<?= Url::toRoute('site/usuarios') ?>">Crear un nuevo usuario</a>


<?php $f = ActiveForm::begin([
   "method" => "get",
   "action" => Url::toRoute("site/viewusuario"),
    "enableClientValidation"=> true,
]); 

?>

<div class ="form-group">
	<?= $f -> field($form,"q")->input("search")?>
</div>

<?= Html::submitButton("Buscar",["class"=> "btn btn-primary"])?>
<?php $f->end()?>

<h3><?= $search ?></h3>

<h3>Lista de usuarios</h3>

<table class="table table-border">
	<tr>
		<th>Id</th>
		<th>Nombre</th>
		<th>Apellido</th>
		<th>Empresa</th>
		<th>Capital</th>
		<th>Contrase�a</th>
		<th></th>
		<th></th>
	
	</tr>	
	<?php foreach($model as $row): ?>
	<tr>
	
		<td><?= $row->id ?></td>
		<td><?= $row->nombre ?></td>
		<td><?= $row->apellido ?></td>
		<td><?= $row->empresa ?></td>
		<td><?= $row->capital ?></td>
		<td><?= $row->contrasena ?></td>
		<td> <a href= "<?= Url::toRoute(["site/modificar","id" => $row->id])?>">Editar</a>
             <a href="#" data-toggle="modal" data-target="#id_<?= $row->id ?>">Eliminar</a>
            <div class="modal fade" role="dialog" aria-hidden="true" id="id_<?= $row->id ?>">
                      <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                    <h4 class="modal-title">Eliminar Usuario</h4>
                              </div>
                              <div class="modal-body">
                                    <p>�Realmente deseas eliminar al usuario con id <?= $row->id ?>?</p>
                              </div>
                              <div class="modal-footer">
                              <?= Html::beginForm(Url::toRoute("site/deleteusuario"), "POST") ?>
                                    <input type="hidden" name="id_alumno" value="<?= $row->id ?>">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-primary">Eliminar</button>
                              <?= Html::endForm() ?>
                              </div>
                            </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->		
		</td>
		<td>
			<a href="#" data-toggle="modal" data-target="#id_<?= $row->id ?>">Eliminar</a>
            <div class="modal fade" role="dialog" aria-hidden="true" id="id_<?= $row->id ?>">
                      <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                    <h4 class="modal-title">Eliminar Usuario</h4>
                              </div>
                              <div class="modal-body">
                                    <p>�Realmente deseas eliminar al alumno con id <?= $row->id ?>?</p>
                              </div>
                              <div class="modal-footer">
                              <?= Html::beginForm(Url::toRoute("site/deleteusuario"), "POST") ?>
                                    <input type="hidden" name="id" value="<?= $row->id ?>">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-primary">Eliminar</button>
                              <?= Html::endForm() ?>
                              </div>
                            </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </td>  
    </tr>

     <?php endforeach ?>
</table>

<?= LinkPager::widget([
    "pagination"=> $pages,
])?>
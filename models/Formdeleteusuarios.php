<?php
namespace app\models;
use Yii;
use yii\base\model;


class FormDeleteUsuarios extends model{
    
    
    public $id;
    public $nombre;
    public $apellido;
    public $empresa;
    public $capital;
    public $contrasena;
    
    public function  rules(){
        
        
        return[
            
           
            ['nombre','match','pattern' =>'/^[a-z������\s]+$/i','message' =>'Solo se acptan letras'],
            ['nombre','match','pattern' =>'/^.{3,50}+$/','message' => 'Escribir entre 3 y 50 caracteres'],
            
            ['apellido','match','pattern' =>'/^[a-z������\s]+$/i','message' =>'Solo se acptan letras'],
            ['apellido','match','pattern' =>'/^.{3,50}+$/','message' => 'Escribir entre 3 y 50 caracteres'],
            
            ['empresa','match','pattern' =>'/^[a-z������\s]+$/i','message' =>'Solo se acptan letras'],
            ['empresa','match','pattern' =>'/^.{3,50}+$/','message' => 'Escribir entre 3 y 50 caracteres'],
            
            ['capital','number','message' => 'solo se admiten numeros enteros positivos'],
            ///^-?[0-9]+([,\.][0-9]*)?$/
            ['capital','match','pattern' => '/^-?[0-9]+([,\.][0-9]*)?$/','message' => 'solo se admiten numeros enteros positivos'],
            
            ['contrasena','match','pattern' =>'/^[a-z������\s]+$/i','message' =>'Solo se acptan letras'],
            ['contrasena','match','pattern' =>'/^.{3,50}+$/','message' => 'Escribir entre 3 y 50 caracteres'], 
        ];
            
        
    }
}


 ?>   
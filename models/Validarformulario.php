<?php 

namespace app\models;

use Yii;
use yii\base\model;


class ValidarFormulario extends model{
    
    public $nombre;
    public $email;
    
    
    public function rules(){
        
        return [
           ['nombre','required','message' => 'Campo requerido'],
           ['nombre','match','pattern' => "/^.{3,50}$/",'message' => 'Introduzca entre 3 y 50 cacteres'],
           ['nombre','match','pattern' => "/.[0-9a-z]+$/i", 'message' => 'No admite caracteres especiales'],
           ['email', 'required','message' => 'Campo requerido'],
           ['email','match','pattern' => "/^.{5,80}$/",'message' => 'Introduzca entre 5 y 8 caracteres'],
           ['email','email','message' => 'Formato no valido'],
        ];
    }
    
    public function attributesLabels(){
        
        return [
            'nombre' => 'Nombre',
            'email' => 'Email',
        ];
    }
}



?>
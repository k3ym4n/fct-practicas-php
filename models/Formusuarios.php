<?php

namespace app\models;
use Yii;
use yii\base\model;


class FormUsuarios extends model{
    
    
   public $id;
   public $nombre;
   public $apellido;
   public $empresa;
   public $capital;
   public $contrasena;
   
   public function rules(){
       
       return[
           
          /* ['id','integer','message' => 'id incorrecto'],
           ['nombre','required','message' => 'Campo requerido'],
           ['nombre','match','pattern' =>'/^[a-z������\s]+$/i','message'=>'S�lo se aceptan letras'],
           ['nombre','match','pattern' =>'/^.{3,50}$/','message'=>'M�nimo 3 m�ximo 50 caracteres'],
           ['apellido','required','message' =>'Campo requerido'],
           ['apellido','match','pattern' =>'/^[a-z������\s]+$/i', 'message' => 'S�lo se aceptan letras'],
           ['apellido','match','pattern' =>'/^.{3,80}$/','message'=>'M�nimo 3 m�ximo 80 caracteres'],
           ['empresa','required','message'=>'Campo requerido'],
           ['empresa','match','pattern' =>'/^[a-z������\s]+$/i','message'=>'S�lo se aceptan letras'],
           ['empresa','match','pattern' =>'/^.{3,80}$/','message'=>'M�nimo 3 m�ximo 80 caracteres'],
           ['contrasena','required','message' =>'Campo requerido'],
           ['contrasena','match','pattern' =>'/^[a-z������\s]+$/i','message'=>'S�lo se aceptan letras'],
           ['contrasena','match','pattern' =>'/^.{3,80}$/','message'=>'M�nimo 3 m�ximo 80 caracteres'],*/
           /*
            * 
            * Esto son los campos de ejemplo incluye un integer para que veas
            * ['clase', 'required', 'message' => 'Campo requerido'],
              ['clase', 'integer', 'message' => 'S�lo n�meros enteros'],
              ['nota_final', 'required', 'message' => 'Campo requerido'],
              ['nota_final', 'number', 'message' => 'S�lo n�meros'],*/
           
           ['id','integer','message'=>'id icorrecta'],
           ['nombre','required','message' => 'Campo necesario'],
           ['nombre','match','pattern' =>'/^[a-z������\s]+$/i','message' =>'Solo se acptan letras'],
           ['nombre','match','pattern' =>'/^.{3,50}+$/','message' => 'Escribir entre 3 y 50 caracteres'],
           ['apellido','required','message' => 'Campo necesario'],
           ['apellido','match','pattern' =>'/^[a-z������\s]+$/i','message' =>'Solo se acptan letras'],
           ['apellido','match','pattern' =>'/^.{3,50}+$/','message' => 'Escribir entre 3 y 50 caracteres'],
           ['empresa','required','message' => 'Campo necesario'],
           ['empresa','match','pattern' =>'/^[a-z������\s]+$/i','message' =>'Solo se acptan letras'],
           ['empresa','match','pattern' =>'/^.{3,50}+$/','message' => 'Escribir entre 3 y 50 caracteres'],
           ['capital','required','message' => 'campo requerido'],
           ['capital','number','message' => 'solo se admiten numeros enteros positivos'],
           ///^-?[0-9]+([,\.][0-9]*)?$/
           ['capital','match','pattern' => '/^-?[0-9]+([,\.][0-9]*)?$/','message' => 'solo se admiten numeros enteros positivos'],
           
           ['contrasena','required','message' => 'Campo necesario'],
           ['contrasena','match','pattern' =>'/^[a-z������\s]+$/i','message' =>'Solo se acptan letras'],
           ['contrasena','match','pattern' =>'/^.{3,50}+$/','message' => 'Escribir entre 3 y 50 caracteres'], 
    
       ];
   }
}
?>
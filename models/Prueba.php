<?php

namespace app\models;

use yii\base\model;

class Prueba extends model{
    public $nombre;
    public $email;
    
    public function rules()
    {
        return [
            ['nombre', 'required', 'message' => 'Campo requerido'],
            ['nombre', 'match', 'pattern' => "/^.{3,50}$/", 'message' => 'M�nimo 3 y m�ximo 50 caracteres'],
            ['nombre', 'match', 'pattern' => "/^[0-9a-z]+$/i", 'message' => 'S�lo se aceptan letras y n�meros'],
            ['email', 'required', 'message' => 'Campo requerido'],
            ['email', 'match', 'pattern' => "/^.{5,80}$/", 'message' => 'M�nimo 5 y m�ximo 80 caracteres'],
            ['email', 'email', 'message' => 'Formato no v�lido'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre:',
            'email' => 'Email:',
        ];
    }
    
}
?>
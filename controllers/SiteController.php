<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\ValidarFormulario;
use app\models\ValidarFormularioAjax;
use yii\widgets\ActiveForm;
use app\models\Usuarios;
use app\models\FormUsuarios;
use app\models\FormDeleteUsuarios;
use app\models\FormSearch;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\data\Pagination;






class SiteController extends Controller//herdada de siteController
{
    /**
     * @inheritdoc
     */
    
    public function actionPruebados(){
        
        return $this->render('p2');
    }
    
    public function actionUpdate(){
        
        $model = new FormUsuarios();
        $msg = null;
        
        if($model->load(Yii::$app->request->post())){
            
            if($model ->validate()){
                
                $table = Usuarios::findOne($model->id);
                
                if($table){
                    
                    $table->nombre = $model->nombre;
                    $table->apellido = $model->apellido;
                    $table->empresa = $model->empresa;
                    $table->capital = $model->capital;
                    $table->contrasena = $model->contrasena;
                    
                    if($table->update()){
                        $msg = "El alumno ha sido actualizado correctamente";
                        
                    }else{
                        
                          $msg = "El alumno no ha podido ser actualizado";
                        }
                }else{
                    $msg = "El alumno seleccionado no ha sido encontrado";
                }
            }
   
        }else{
            $model->getErrors();
        }
        
        return $this->render('modificar');
    }
    
    public function actionDelete(){
        
        if(Yii::$app->request->post()){
            
            $id = Html::encode($_POST["id"]);
            
            if((int) $id){
                
                if(Usuarios::deleteAll("id=:id" , [":id" => $id])){
                    
                    echo " El usuario con id $id ha sido eliminado con exito,resireccionando...";
                    echo " <meta http-equiv='refresh content='3; ".Url::toRoute("site/viewusuario")."'>";
                    
                }else{
                    
                    echo " Ha ocurrido un ERROR ,el usuario con id $id NO ha sido eliminado ,resireccionando...";
                    echo " <meta http-equiv='refresh content='3; ".Url::toRoute("site/viewusuario")."'>";
                }
                
            }else{
                
                echo " Ha ocurrido un ERROR ,el usuario con id $id NO ha sido eliminado ,resireccionando...";
                echo " <meta http-equiv='refresh content='3; ".Url::toRoute("site/viewusuario")."'>";
            }
           
                
        }else{
            
           return $this->redirect(["site/viewusuario"]);
            
        }
    }
    
    public function actionViewusuario(){
       /*Sirve para listar una tabla y buscra un usuario por id nombre y apellido*/
      /*  $table = new Usuarios();
        $model = $table ->find()->all();
        
        $form = new FormSearch();
        $search = null;
        
        if($form->load(Yii::$app->request->get())){
            
            if($form->validate()){
                $search = Html::encode($form->q);
                $query = "SELECT * FROM usuarios WHERE id LIKE '%$search%' OR ";
                $query .= "nombre LIKE '%$search%' OR apellido LIKE '%$search%'";
                $model = $table->findBySql($query)->all();
            }else{
                
                $form->getErrors();
            }
        }*/
        
        $form = new FormSearch();
        $search = null;
        
        if($form->load(Yii::$app->request->get())){
            
            if($form->validate()){
               
                $search = Html::encode($form->q);
                $table = Usuarios::find()
                ->where(["like","id",$search])
                ->orwhere(["like","nombre",$search])
                ->orwhere(["like","apellido",$search]);
                
                $count = clone $table;
                $pages = new Pagination([
                    "pageSize"=> 25,
                    "totalCount" => $count->count()
                    
                ]);
                
                $model = $table 
                ->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
            }else{
                
                $form->getErrors();
            }
        }else{
            
            $table = Usuarios::find();
            $count = clone $table;
            $pages = new Pagination([
                "pageSize" => 25,
                "totalCount" => $count->count()
            ]);
            
            $model = $table
            -> offset($pages->offset)
            ->limit($pages->limit)
            ->all();
            
        }
        
        
        return $this->render('viewusuario',
                ['model' => $model,
                 'form'=>$form ,
                 'search'=>$search, 
                 'pages'=> $pages]);
        
    }
    /**
     * Metodo para la conexion de la base de datos;
     */
    public function actionUsuarios(){
        $model = new FormUsuarios();
        
        $msg = null;
        
        if ($model->load(Yii::$app->request->post())) {
            
            if ($model->validate()) {
                
                $table = new Usuarios();
                
                $table->nombre = $model->nombre;
                $table->apellido = $model->apellido;
                $table->empresa = $model->empresa;
                $table->capital = $model->capital;
                $table->contrasena = $model->contrasena;
                
                if ($table->insert()) {
                    $msg = 'Muy bien david has logrado introducir tus primeros datos mediante PHP';
                    
                    $model->nombre = null;
                    $model->apellido = null;
                    $model->empresa = null;
                    $model->capital = null;
                    $model->contrasena = null;
                } else {
                    $msg = "Ha ocurrido un error,mira que eres torpe";
                }
            } else {
                
                $model->getErrors();
            }
            
        }
        return $this->render('usuarios', [
            'model' => $model,
            'msg' => $msg
        ]);
    }
    
    public function actionDeleteusuario(){
        
        $model = new FormDeleteUsuarios();
        $msg = null;
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->delete()) {
                
                $msg = 'Usuario eliminado';
                
                $model->nombre = null;
                $model->apellido = null;
                $model->empresa = null;
                $model->capital = null;
                $model->contrasena = null;
            } else {
                $msg = "Ha ocurrido un error, XD";
            }
        }
        
        return $this->render('deleteusuario', [
            'model' => $model,
            'msg' => $msg
        ]);
        
        
    }
        
    
    /**
     * esto es como un metodo en java que pasa un parametro en este caso de texto
     * y numeros y ademas una variable propia del metodo ($get) que hemos inicializado
     * cada variable se puede usar en ela view/site saludar.php
     */
    public function actionSaludar($get = "Tutoria Yii"){
        
        $mensaje = "Fuck the worlD!!!!";
        $numeros = [0,1,2,3,4,5,6,7,8,9];
        return $this->render("saludar", [
            "mensaje" => $mensaje,
            "arraynum" => $numeros,
            "get" => $get
            
        ]);
    }
    
    public function actionFormulario($mensaje = null){
        
        return $this->render('formulario' , ["mensaje" => $mensaje]);
    }
    
    public function actionRequest(){
        $mensaje = null;
        if(isset($_REQUEST["nombre"])){
            //este mensaje se lo vamos a pasr a la vista formulario
            $mensaje  = 'Enviado correctamente tu nombre: ' .$_REQUEST["nombre"];
        }
        return $this->redirect(['site/formulario',"mensaje"=> $mensaje]);
    }
    
    
   public function actionValidarformulario(){
       
       $model = new ValidarFormulario();
       
       if($model ->load(Yii::$app->request->post())){
           
           if($model->validate()){
               
               //TODO comprpbar con una DB
           }else{
               $model->getErrors();
           }
       }
       
       return $this->render('validarformulario' , ["model" => $model]);
              
   }
   
   public function actionValidarformularioajax(){
       $model = new ValidarFormularioAjax();
       $msg = null;
       
       if($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax){
           
           Yii::$app->response->format = Response::FORMAT_JSON;
           Return ActiveForm::validate($model);
       }
       
       if($model ->load(Yii::$app->request->post())){
           
           if($model->validate()){
               
               //TODO comprpbar con una DB
               $msg = 'Enhorabuena formulario enviado';
               $model->nombre=null;
               $model->email=null;
           }else{
               $model->getErrors();
           }
       }
       
       return $this->render('validarformularioajax' , ["model" => $model,'msg' => $msg]);
   }
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
